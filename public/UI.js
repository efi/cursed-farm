const UI = {}
UI.content = $("content")
UI.header = $("<header>Cursed Farm <small>by efi</small> <red>[WIP]</red></header>")
UI.main = $("<main></main>")
UI.nav = $("<tabs id=\"navbar\"></tabs>")
UI.buttonarray = $("<buttonarray></buttonarray>")
UI.resourcepane = $("<resources></resources>")
UI.textpane = $("<textlog></textlog>")
UI.init = () => {
  UI.nav.on("click", "button.tab", UI.switchtab)
  UI.content
    .append(UI.header)
    .append(UI.toolbar)
    .append(UI.nav)
    .append(UI.main)
    .append(UI.textpane)
  UI.content.prependTo("body")
  _.each(DATA.tabs, (t, k) => {
    UI.nav.append(UI.tab.clone().html(t.label).data("key", k).hide())
    UI.main.append($(`<tab id="${k}"></tab>`).hide())
  })
  UI.resbuttons    = UI.buttonarray.clone()
  UI.crebuttons    = UI.buttonarray.clone()
  UI.buibuttons    = UI.buttonarray.clone()
  UI.crabuttons    = UI.buttonarray.clone()
  UI.potbuttons    = UI.buttonarray.clone()
  UI.researchbutts = UI.buttonarray.clone()
  $("#resources").show()
  _.each(DATA.resources, (t, k) => {
    let b = UI.button.clone(true).data("key", k)
    b.children("label").html(t.buyLabel)
    b.children("icon").css("background-position", `-${32 * (t.icon % 10)}px -${32 * Math.floor(t.icon / 10)}px`)
    b.hide()
    switch(t.cat) {
      case "resources":
        UI.resbuttons.append(b)
        break
      case "creatures":
        UI.crebuttons.append(b)
        break
      case "buildings":
        UI.buibuttons.append(b)
        break
      case "crafting":
        UI.crabuttons.append(b.addClass("crafting"))
        break
    }
    let r = UI.res.clone(true).data("key", k)
    r.children("amount").data("key", k)
    r.children("label").html(t.label)
    r.hide()
    UI.resourcepane.append(r)
  })
  _.each(DATA.research, (t, k) => {
    let b = UI.button.clone(true).data("key", k).addClass("research")
    b.children("label").html(t.buyLabel)
    b.children("icon").css("background-position", `-${32 * (t.icon % 10)}px -${32 * Math.floor(t.icon / 10)}px`)
    b.hide()
    UI.researchbutts.append(b)
  })
  _.each(_.range(25), (v) => {
    UI.ritualButton.clone().data("n", v).appendTo(UI.ritualGrid)
  })
  _.each(DATA.potionIngredients, (k) => {
    UI.potionButton.clone(true)
      .data("key", k)
      .find("label").html(k == "water" ? "Water" : DATA.resources[k].label).end()
      .appendTo(UI.potbuttons)
  })
  UI.resbuttons   .on("click", ".action", UI.buyOne)
  UI.crebuttons   .on("click", ".action", UI.buyOne)
  UI.buibuttons   .on("click", ".action", UI.buyOne)
  UI.crabuttons   .on("click", ".action", UI.buyOne)
  UI.potbuttons   .on("click", ".action", UI.addIngredient)
  UI.researchbutts.on("click", ".action", UI.researchThis)
  UI.ritualGrid   .on("click", "button", UI.ritualClick)
  UI.ritualRestart.on("click", ACT.newRitual)
  UI.ritualRoll   .on("click", ACT.ritualRoll)
  UI.ritualClaim  .on("click", ACT.ritualClaim)
  $("#rituals")  .append(UI.ritualGrid)
                 .append(UI.ritualRestart)
                 .append(UI.ritualRoll)
                 .append(UI.ritualClaim)
                 .append(UI.ritualAltar)
  $("#resources").append(UI.resbuttons)
  $("#creatures").append(UI.crebuttons)
  $("#buildings").append(UI.buibuttons)
  $("#crafting") .append(UI.crabuttons)
  $("#potions")  .append(UI.potbuttons)
  $("#research") .append(UI.researchbutts)
  UI.main.append(UI.resourcepane)
  UI.content.on("click", "spinner .increase", UI.spinnerUp)
  UI.content.on("click", "spinner .decrease", UI.spinnerDown)
  UI.toolbar.append(UI.temp).append(UI.clit).append(UI.affn)
}
UI.button = $("<button class=\"action\"><icon></icon><label></label><price></price></button>")
UI.ritualButton = $("<button class=\"ritual\"><label></label></button>")
UI.ritualGrid = $("<ritual></ritual>")
UI.ritualAltar = $("<altar></altar>")
UI.ritualRestart = $(`<button id="restart">${DATA.globals.symbols.restart} Restart</button>`)
UI.ritualRoll = $(`<button id="reroll">${DATA.globals.symbols.reroll} Continue</button>`)
UI.ritualClaim = $(`<button id="claim">${DATA.globals.symbols.claim} Perform</button>`)
UI.potionButton = UI.button.clone(true).addClass("potion")
UI.tab = $("<button class=\"tab\"></button>")
UI.res = $("<res><amount></amount><label></label></res>")
UI.toolbar = $("<toolbar></toolbar>")
UI.cat = "resources"
UI.switchtab = (ev) => {
  let K = $(ev.currentTarget).data("key")
  $("tab").hide()
  $(`tab#${K}`).show()
  UI.cat = K
}
UI.update = () => {
  $("button.tab").each((i, e) => {
    let E = $(e)
    if (DATA.tabs[E.data("key")].show()) {
      E.show()
    } else {
      E.hide()
    }
  })
  $("button.action").each((i, e) => {
    let E = $(e)
    let K = E.data("key")
    if (K == "water") {return}
    let From = "resources"
    if (E.hasClass("research")) {From = "research"}
    let R = DATA[From][K]
    if (R.cat != UI.cat) {return}
    if (R.cat == "research" && GAME.isUnlocked(K)) {
      return E.disable()
    }
    if (!_.isUndefined(R.showButton) && R.showButton()) {
      E.show()
      E.children("price").html(UI.renderPrice(DATA[From][K].price))
      if (ACT.canBuy(DATA[From][K].price)) {
        E.enable()
      } else {
        E.disable()
      }
    } else {
      E.hide()
    }
  })
  $("res").each((i, e) => {
    let E = $(e)
    let R = DATA.resources[E.data("key")]
    if (!_.isUndefined(R.showLabel) && R.showLabel()) {
      E.show()
    } else {
      E.hide()
    }
  })
  $("res amount").each((i, e) => {
    let E = $(e)
    E.html(UI.numtoimg(UI.shortnum(GAME.resValue(E.data("key")))))
  })
  UI.tempupd()
  UI.clitupd()
  UI.affnupd()
}
UI.buyOne = (ev) => {
  ACT.buyMany($(ev.currentTarget).data("key"), 1)
}
UI.researchThis = (ev) => {
  ACT.research($(ev.currentTarget).data("key"))
}
UI.addIngredient = (ev) => {
  let K = $(ev.currentTarget).data("key")
  if (K == "water") {
    GAME.cauldronState.water += 1
  } else {
    GAME.resources[K] -= 1
    GAME.cauldronState[K] += 1
  }
}
UI.ritualClick = (ev) => {
  if (!GAME.ritualRolling) {
    let n = $(ev.currentTarget).data("n")
    $(ev.currentTarget).disable()
    ACT.lockRitualSlot(n)
  }
}
UI.renderPrice = (price) => {
  return _.isEmpty(price) ?
         "" :
         " (" + _.map(price,
                      (v, k) => DATA.resources[k].label + " x" + UTIL.fnorval(v))
                 .join(", ") + ")"
}
UI.number = $("<number></number>")
UI.mononumber = {
  black:
    _.map(_.range(10), (c) => {
      return UI.number.clone().css("background-position", `-${11 * c}px 0px`)
    }).concat(UI.number.clone().css("background-position", "-110px 0px")),
  white:
    _.map(_.range(10), (c) => {
      return UI.number.clone().css("background-position", `-${11 * c}px -16px`)
    }).concat(UI.number.clone().css("background-position", "-110px -16px")),
}
UI.numtoimg = (s, white=false) => {
  return _.map(String(s), (c) => {
      if (isNaN(Number(c)) && c != '.') {return c}
      if (c == '.') {return UI.mononumber[white?"white":"black"][10][0].outerHTML}
      return UI.mononumber[white?"white":"black"][Number(c)][0].outerHTML
    }).join("")
}
UI.shortnum = (n) => {
  n = Math.floor(Number(n))
  if (n <= 0) {return 0}
  let l = Math.floor(Math.log10(n) / 3)
  if (l <= 7) {
    return (l > 0 ? ">" : "") +
           ((Math.floor(10 * (n / Math.pow(1000, l)))) / 10).toFixed(l > 0 ? 1 : 0) +
           ["", "K", "M", "G", "T", "E", "Z", "Y"][l]
  }
  return n.toExponential(4)
}
UI.textline = $("<line><count></count></line>")
UI.text = (m) => {
  UI.textpane.append(UI.textline.clone(true).prepend(m).data("type", m))
  UI.compact()
  UI.textpane.scrollTop(Number.MAX_SAFE_INTEGER / 10)
}
UI.compact = () => {
  if (UI.textpane.children().last().data("type") == UI.textpane.children().last().prev().data("type")) {
    UI.textpane.children().last().remove()
    let c = UI.textpane.children().last().children("count")
    let n = (c.data("count") || 1) + 1
    c.html(` (x${n})`).data("count", n)
  }
}
UI.spinner = $(`<spinner><label></label><div>Level: <button class="decrease">&#9664;</button><value>${UI.numtoimg(0, true)}</value><button class="increase">&#9654;</button> Amount used: <result>${UI.numtoimg(0, true)}</result></div></spinner>`).data("v", 0)
UI.spinnerUp = (ev) => {
  w = $(ev.currentTarget).parent()
  v = Math.min(10, w.parent().data("v") + 1)
  w.parent().data("v", v)
  w.find("value").html(UI.numtoimg(v, true))
  w.find("result").html(UI.numtoimg(Math.pow(5, v), true))
}
UI.spinnerDown = (ev) => {
  w = $(ev.currentTarget).parent()
  v = Math.max(0, w.parent().data("v") - 1)
  w.parent().data("v", v)
  w.find("value").html(UI.numtoimg(v, true))
  w.find("result").html(UI.numtoimg(v == 0 ? 0 : Math.pow(5, v - 1), true))
}
UI.temp = $('<thermal><icon>🌡</icon><value>--</value></thermal>')
UI.clit = $('<clit><icon>🌥</icon><value>Sunny</value></clit>')
UI.affn = $('<affinity><icon>⚝</icon><div><icon>🜁</icon><value></value><icon>🜂</icon><value></value><icon>🜃</icon><value></value><icon>🜄</icon><value></value></div></affinity>')
UI.tempupd = () => {
  if (GAME.isUnlocked("thermometer")) {
    UI.temp.find("value").html(`${UTIL.numtoimg(GAME.currentTemp.toFixed(1))}°C`)
  } else {
    UI.temp.find("value").html(UTIL.tempword(GAME.currentTemp))
  }
}
UI.clitupd = () => {
}
UI.affnupd = () => {
  if (GAME.isUnlocked("rituals")) {
    UI.affn.fadeIn()
    UI.affn.find("value")
      .eq(0).html(GAME.elementalAffinity.aer).end()
      .eq(1).html(GAME.elementalAffinity.ignis).end()
      .eq(2).html(GAME.elementalAffinity.terra).end()
      .eq(3).html(GAME.elementalAffinity.aqua).end()
  } else {
    UI.affn.hide()
  }
}
UI.hi       = (e) => { $(e).addClass("hi")      }
UI.unhi     = (e) => { $(e).removeClass("hi")   }
UI.red      = (e) => { $(e).addClass("red")     }
UI.unred    = (e) => { $(e).removeClass("red")  }
UI.cursed   = (e) => { $(e).addClass("cursed")     }
UI.uncursed = (e) => { $(e).removeClass("cursed")  }
