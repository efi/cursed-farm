const UTIL = {}
UTIL.fnorval = (v) => _.isFunction(v) ? v() : v
UTIL.posClamp = (n) => Math.max(n, 0)
UTIL.posInt = (n) => Math.floor(UTIL.posClamp(n))
UTIL.choice = (...a) => _.sample(a)
UTIL.isUnlocked = function (...locks) {return () => _.every(locks, GAME.isUnlocked)}
UTIL.randGend = () => UTIL.choice(["He", "Him", "His"], ["She", "Her", "Her"], ["They", "Them", "Their"])
UTIL.grid = (x, y) => {
  let G = {
    x: x,
    y: y,
    l: x * y,
  }
  return Object.assign(G, {
    E: (n) => (n % G.x < G.x - 1) ? n + 1 : null,
    S: (n) => (n + G.x < G.l) ? n + G.x : null,
    SE: (n) => (n % G.x < G.x - 1 && n + G.x < G.l) ? n + G.x + 1 : null,
    SW: (n) => (n % G.x > 0 && n + G.x < G.l) ? n + G.x - 1 : null,
  })
}
UTIL.weightedChoice = (weights) => {
  let W = _.mapObject(weights, (i) => i())
  let size = _.reduce(W, (a, b) => a + b, 0)
  let spot = Math.random() * size
  return _.reduce(W, (m, v, k) => _.isString(m) ? m : ((m + v >= spot) ? k : m + v), 0)
}
UTIL.ritualSymbol = () => {
  return UTIL.weightedChoice(DATA.ritualSymbolWeights)
}
UTIL.tempword = (temp) => _.chain(DATA.globals.tempwords).pick((v) => Number(v) <= temp).keys().last().value() || "Impossibly cold"
UTIL.currentZodiac = () => DATA.globals.zodiac[Math.floor(GAME.starAlignment % 13)]
UTIL.isMix = (iA, iB, epsilon) => {
  A = _(iA).pairs().reject((p) => p[1] == 0).object().value()
  B = _(iB).pairs().reject((p) => p[1] == 0).object().value()
  sumA = _(A).values().reduce((a, b) => a + b).value()
  sumB = _(B).values().reduce((a, b) => a + b).value()
  ratioA = _.mapObject(A, (v) => v / sumA)
  ratioB = _.mapObject(B, (v) => v / sumB)
  common = _.intersection(_.keys(A), _.keys(B))
  if (!_(B).keys().difference(common).isEmpty().value()) {return false}
  return _.every(common, (k) => Math.abs(ratioA[k] - ratioB[k]) <= epsilon)
}
