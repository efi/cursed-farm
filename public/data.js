let Free = {}
let Always = _.constant(true)
let Never = _.constant(false)
let C = UTIL.choice
let isUnlocked = UTIL.isUnlocked

const DATA = {}
DATA.intro = "You stand in the middle of a dense forest. Around you, a field of red mushrooms in full bloom. These are what you came here for."
DATA.globals = {
  tempwords: {
    "Extremely freezing":  "-273",
    "Freezing cold":       "-30",
    "Cold":                "0",
    "Frigid":              "5",
    "Chilly":              "10",
    "Comfortable":         "15",
    "Tepid":               "20",
    "Warm":                "25",
    "Sweltering":          "30",
    "Hot":                 "35",
    "Extremely hot":       "40",
    "Deadly hot":          "45",
    "Hellish hot":         "60",
    "Boiling hot":         "100",
  },
  zodiac: ["aries", "taurus", "gemini", "cancer", "leo", "virgo", "libra", "scorpio", "sagittarius", "capricorn", "aquarius", "pisces"], 
  symbols: {
    mercury:      "☿",
    venus:        "♀",
    earth:        "♁",
    mars:         "♂",
    jupiter:      "♃",
    saturn:       "♄",
    uranus:       "♅",
    neptune:      "♆",
    pluto:        "♇",
    quintessence: "🜀",
    aer:          "🜁",
    ignis:        "🜂",
    terra:        "🜃",
    aqua:         "🜄",
    aquafortis:   "🜅",
    aquaregia:    "🜆",
    aquavitae:    "🜉",
    brimstone:    "🜏",
    vitriol:      "🜖",
    salt:         "🜘",
    gold:         "🜚",
    silver:       "🜛",
    iron:         "🜜",
    copper:       "🜠",
    tin:          "🜩",
    lead:         "🜪",
    antimony:     "🜫",
    wax:          "🝊",
    ankh:         "☥",
    aries:        "♈",
    taurus:       "♉",
    gemini:       "♊",
    cancer:       "♋",
    leo:          "♌",
    virgo:        "♍",
    libra:        "♎",
    scorpio:      "♏",
    sagittarius:  "♐",
    capricorn:    "♑",
    aquarius:     "♒",
    pisces:       "♓",
    restart:      "♲",
    reroll: "🔄",
    claim: "🎫",
  },
}
DATA.tabs = {
  resources:  {label: "Resources",  show: Always},
  creatures:  {label: "Creatures",  show: isUnlocked("gnome")},
  buildings:  {label: "Buildings",  show: isUnlocked("farm1")},
  crafting:   {label: "Crafting",   show: isUnlocked("crafting")},
  research:   {label: "Research",   show: isUnlocked("magicDust")},
  rituals:    {label: "Rituals",    show: isUnlocked("rituals")},
  potions:    {label: "Potions",    show: isUnlocked("potions")},
  spells:     {label: "Spells",     show: isUnlocked("spells")},
  deities:    {label: "Deities",    show: isUnlocked("deities")},
  trade:      {label: "Trade",      show: isUnlocked("trade")},
},
DATA.resources = {
  // Resources
  amanita: {
    cat: "resources",
    price: Free,
    showLabel: Always,
    showButton: Always,
    label: "Amanitas",
    buyLabel: "Pick up some amanitas",
    buyText: "Gathered a few mushrooms.",
    tip: "A gnome's favorite mushroom.",
    icon: 0,
    //buyFn: (amount) => amount * ((Math.random() * 3) + 1),
  },
  wood: {
    cat: "resources",
    showLabel: isUnlocked("gnome"),
    showButton: Never,
    label: "Wood",
    tip: "Useful to build many things.",
  },
  belladona: {
    cat: "resources",
    price: Free,
    showLabel: isUnlocked("belladona"),
    showButton: isUnlocked("belladona"),
    label: "Belladona Flowers",
    buyLabel: "Gather belladona",
    buyText: "Gathered some belladona flowers.",
    tip: "A delicate flower that withers soon.",
    icon: 1,
  },
  magicDust: {
    cat: "resources",
    showLabel: isUnlocked("magicDust"),
    showButton: Never,
    label: "Magic Dust",
    icon: 2,
  },
  slime: {
    cat: "resources",
    price: {magicDust: 8},
    showLabel: isUnlocked("slime"),
    showButton: isUnlocked("slime"),
    label: "Slime",
    buyLabel: "Make slime",
    buyText: "Made some slime.",
    tip: "Goopy.",
    icon: 3,
  },
  bone: {
    cat: "resources",
    price: {gnome: () => GAME.resValue("troll") * 6},
    showLabel: isUnlocked("bone"),
    showButton: isUnlocked("trolls"),
    label: "Bones",
    buyLabel: "Gather bones",
    buyText: "Fed gnomes to the trolls. Took their bones.",
    icon: 4,
  },
  hemlock: {
    cat: "resources",
    price: Free,
    showLabel: isUnlocked("hemlock"),
    showButton: isUnlocked("hemlock"),
    label: "Hemlock Flowers",
    buyLabel: "Gather hemlock",
    buyText: "Gathered some hemlock flowers.",
    tip: "A very poisonous flower.",
    icon: 5,
  },
  wormwood: {
    cat: "resources",
    price: Free,
    showLabel: isUnlocked("wormwood"),
    showButton: isUnlocked("wormwood"),
    label: "Wormwood",
    buyLabel: "Gather wormwood",
    buyText: "Gathered some wormwood.",
  },
  ginseng: {
    cat: "resources",
    price: Free,
    showLabel: isUnlocked("ginseng"),
    showButton: isUnlocked("ginseng"),
    label: "Ginseng Roots",
    buyLabel: "Dig up ginseng",
    buyText: "Dug up a ginseng root.",
  },
  skull: {
    cat: "resources",
    label: "Skulls",
    showButton: Never,
  },
  woodenSupport: {
    cat: "resources",
    price: {wood: 12},
    showLabel: isUnlocked("mines"),
    showButton: isUnlocked("mines"),
    label: "Wooden supports",
    buyLabel: "Construct wooden supports",
    buyText: "Constructed wooden supports.",
    tip: "Essential structures for excavating mines.",
  },
  stone: {
    cat: "resources",
    label: "Stone",
    showButton: Never,
  },
  sand: {
    cat: "resources",
    label: "Sand",
    showButton: Never,
  },
  gems: {
    cat: "resources",
    label: "Gems",
    showButton: Never,
  },
  manaCrystal: {
    cat: "resources",
    label: "Mana crystals",
    showButton: Never,
  },
  ectoplasm: {
    cat: "resources",
    label: "Ectoplasm",
  },
  antimony: {
    cat: "resources",
    label: "Antimony",
  },
  salt: {
    cat: "resources",
    label: "Salt",
  },
  wax: {
    cat: "resources",
    label: "Wax",
  },
  cinnabar: {
    cat: "resources",
    label: "Cinnabar",
  },
  mercury: {
    cat: "resources",
    label: "Mercury",
  },
  coal: {
    cat: "resources",
    label: "Coal",
  },
  sulfur: {
    cat: "resources",
    label: "Sulphur",
  },
  tablet: {
    cat: "resources",
    label: "Undecipherable glyph tablets",
  },
  blood: {
    cat: "resources",
    label: "Blood",
  },
  wheat: {
    cat: "resources",
    label: "Wheat",
  },
  bread: {
    cat: "resources",
    label: "Bread",
  },
  souls: {
    cat: "resources",
    label: "Souls",
  },
  // Creechures
  gnome: { // eat amanita, make wood
    cat: "creatures",
    price: {amanita: 6},
    showLabel: isUnlocked("gnome"),
    showButton: isUnlocked("gnome"),
    label: "Gnomes",
    buyLabel: "Hire a gnome",
    buyText: "Hired a gnome.",
    tip: "Eats mushrooms and brings wood. Not a terrible deal.",
    icon: 2,
  },
  fairy: { // make magicDust if there's belladona
    cat: "creatures",
    price: {belladona: 20, wood: 5},
    showLabel: isUnlocked("fairy"),
    showButton: isUnlocked("fairy"),
    label: "Fairies",
    buyLabel: "Catch a fairy",
    buyText: "Caught a fairy.",
    tip: "Fickle creature hard to keep trapped. Its wings shed magic dust when it's surrounded with belladona flowers.",
  },
  goblin: { // eat shrooms, make mines
    cat: "creatures",
    label: "Goblins",
  },
  kobold: { // eat gems, make wheat
    cat: "creatures",
    label: "Kobolds",
  },
  troll: { // eat goblins, make bones
    cat: "creatures",
    label: "Trolls",
  },
  imp: { // eat souls, unlock magic crafting
    cat: "creatures",
    label: "Imps",
    showLabel: isUnlocked("imps"),
    showButton: Never,
  },
  demon: { // eats sulfur, improves stuff
    cat: "creatures",
    label: "Demons",
  },
  succubus: { // eats coins, improves stuff
    cat: "creatures",
    label: "Succubi",
  },
  cat: { // give bad luck
    cat: "creatures",
    label: "Cats",
    showLabel: isUnlocked("cats"),
    showButton: Never,
    tip: "meow =3",
  },
  villager: { // eat bread
    cat: "creatures",
    lavel: "Villagers",
  },
  dragon: { // eat kobolds, make coins
    cat: "creatures",
    label: "Dragons",
  },
  ghost: { // make ectoplasm
    cat: "creatures",
    label: "Ghots",
  },
  satyr: { // eat music, make bread
    cat: "creatures",
    label: "Satyrs",
  },
  slimecube: { // eat slime, make slime cubes
    cat: "creatures",
    label: "Slime cubes",
  },
  slimegirl: { // eat slime, make things
    cat: "creatures",
    label: "Slime girls",
  },
  dwarf: { // eats metals, unlock machines
    cat: "creatures",
    label: "Dwarves",
  },
  naga: { // eat villagers, makes manaCrystals
    cat: "creatures",
    label: "Nagas",
  },
  lamia: { // eat villagers, makes blood
    cat: "creatures",
    label: "Lamias",
  },
  nymph: { // eat villagers, make music
    cat: "creatures",
    label: "Nymphs",
  },
  undine: { // eat villagers, unlock leylines
    cat: "creatures",
    label: "Undines",
  },
  dryad: { // eat villagers, boost plants
    cat: "creatures",
    label: "Dryads",
  },
  mermaid: { // eat villagers, unlock islands
    cat: "creatures",
    label: "Mermaids",
  },
  harpy: { // eat villagers, produce eggs
    cat: "creatures",
    label: "Harpies",
  },
  owl: { // eat paper, make blankScrolls
    cat: "creatures",
    label: "Owls",
  },
  shadow: { // eat souls
    cat: "creatures",
    label: "Living shadows",
  },
  skeleton: { // from bones
    cat: "creatures",
    label: "Skeletons",
  },
  rat: {
    cat: "creatures",
    label: "Rat titans",
  },
  boar: {
    cat: "creatures",
    label: "Boar titans",
  },
  deer: {
    cat: "creatures",
    label: "Deer titans",
  },
  beetle: {
    cat: "creatures",
    label: "Sacred beetles",
  },
  // Buildings
  amanitaFarm: {
    cat: "buildings",
    price: {amanita: 12, wood: 24},
    showLabel: isUnlocked("farm1"),
    showButton: isUnlocked("farm1"),
    label: "Amanita farms",
    buyLabel: "Build a mushroom farm",
    buyText: "Built an amanita farm.",
    tip: "Grows mushrooms over time for free.",
    icon: 12,
  },
  bonfire: {
    cat: "buildings",
    price: {wood: 8},
    showLabel: isUnlocked("bonfire"),
    showButton: isUnlocked("bonfire"),
    label: "Bonfire strength",
    buyLabel: "Add wood to the fire",
    buyText: "Added wood to the fire.",
  },
  belladonaFarm: {
    cat: "buildings",
    label: "Belladona farms",
  },
  hemlockFarm: {
    cat: "buildings",
    label: "Hemlock farms",
  },
  wormwoodFarm: {
    cat: "buildings",
    label: "Wormwood farms",
  },
  ginsengFarm: {
    cat: "buildings",
    label: "Ginseng farms",
  },
  wheatFarm: {
    cat: "buildings",
    label: "Wheat farms",
  },
  wheatMill: {
    cat: "buildings",
    label: "Wheat mills",
  },
  abstractMine: {
    cat: "buildings",
    price: {wood: 400},
    showLabel: Never,
    buyLabel: "Excavate a mine",
    buyText: "Dug deeper.",
  },
  mineCoal: {
    cat: "buildings",
    label: "Coal mines",
  },
  mineCopper: {
    cat: "buildings",
    label: "Copper mines",
  },
  mineTin: {
    cat: "buildings",
    label: "Tin mines",
  },
  mineZinc: {
    cat: "buildings",
    label: "Zinc mines",
  },
  mineIron: {
    cat: "buildings",
    label: "Iron mines",
  },
  mineSilver: {
    cat: "buildings",
    label: "Silver mines",
  },
  mineGold: {
    cat: "buildings",
    label: "Gold mines",
  },
  mineLead: {
    cat: "buildings",
    label: "Lead mines",
  },
  mineCinnabar: {
    cat: "buildings",
    label: "Cinnabar mines",
  },
  mineAntimony: {
    cat: "buildings",
    label: "Antimony mines",
  },
  mineSulfur: {
    cat: "buildings",
    label: "Sulphur mines",
  },
  mineManaCrystal:  {
    cat: "buildings",
    label: "Mana crystal mines",
  },
  graveyard: {
    cat: "buildings",
    label: "Graveyards",
  },
  // Summoning circles
  summonImp: {
    cat: "summoning",
    label: "Imp summoning circles",
  },
  summonDemon: {
    cat: "summoning",
    label: "Demon summoning circles",
  },
  summonSuccubus: {
    cat: "summoning",
    label: "Succubus summoning circles",
  },
  summonGhost: {
    cat: "summoning",
    label: "Ghost summoning circles",
  },
  summonNaga: {
    cat: "summoning",
    label: "Naga summoning circles",
  },
  summonLamia: {
    cat: "summoning",
    label: "Lamia summoning circles",
  },
  summonNymph: {
    cat: "summoning",
    label: "Nymph summoning circles",
  },
  summonUndine: {
    cat: "summoning",
    label: "Undine summoning circles",
  },
  summonDryad: {
    cat: "summoning",
    label: "Dryad summoning circles",
  },
  summonMermaid: {
    cat: "summoning",
    label: "Mermaid summoning circles",
  },
  summonOwl: {
    cat: "summoning",
    label: "Owl summoning circles",
  },
  summonShadow: {
    cat: "summoning",
    label: "Shadow summoning circles",
  },
  // Crafting
  copper: {
    cat: "crafting",
    label: "Copper",
    showButton: isUnlocked("transmutation"),
  },
  zinc: {
    cat: "crafting",
    label: "Zinc",
    showButton: isUnlocked("transmutation"),
  },
  brass: {
    cat: "crafting",
    label: "Brass",
    showButton: isUnlocked("brass"),
  },
  iron: {
    cat: "crafting",
    label: "Iron",
    showButton: isUnlocked("transmutation"),
  },
  steel: {
    cat: "crafting",
    label: "Steel",
    showButton: isUnlocked("steel"),
  },
  silver: {
    cat: "crafting",
    label: "Silver",
    showButton: isUnlocked("transmutation"),
  },
  lead: {
    cat: "crafting",
    label: "Lead",
    showButton: isUnlocked("transmutation"),
  },
  gold: {
    cat: "crafting",
    label: "Gold",
    showButton: isUnlocked("transmutation"),
  },
  glass: {
    cat: "crafting",
    label: "Glass",
    showButton: Never,
  },
  enchantedGlass: {
    cat: "crafting",
    label: "Enchanted glass",
  },
  paper: {
    cat: "crafting",
    label: "Paper",
  },
  blankScroll: {
    cat: "crafting",
    label: "Blank scrolls",
  },
  book: {
    cat: "crafting",
    label: "Books",
  },
  grimoire: {
    cat: "crafting",
    label: "Grimoires",
  },
  copperCoin: {
    cat: "crafting",
    label: "Copper coins",
  },
  silverCoin: {
    cat: "crafting",
    label: "Silver coins",
  },
  goldCoin: {
    cat: "crafting",
    label: "Gold coins",
  },
  tin: {
    cat: "crafting",
    label: "Tin",
  },
  bronze: {
    cat: "crafting",
    label: "Bronze",
  },
  vitriol: {
    cat: "crafting",
    label: "Vitriol",
  },
  spirit: {
    cat: "crafting",
    label: "Spirit",
  },
  carbon: {
    cat: "crafting",
    label: "Carbon",
  },
}
DATA.research = {
  slime: {
    cat: "research",
    price: {magicDust: 8},
    buyLabel: "Slime",
    showButton: isUnlocked("magicDust"),
    buyText: "Pouring some magic dust on the forest mud gave it life! How curious...",
    tip: "Goopy stuff!",
  },
  bonfire: {
    cat: "research",
    price: {wood: 80},
    buyLabel: "Bonfire",
    showButton: Always,
    buyText: "You lit up a bonfire. It's warm and cozy.",
    tip: "Cozy",
  },
  potions: {
    cat: "research",
    price: {magicDust: 20},
    buyLabel: "Potions",
    showButton: isUnlocked("magicDust"),
    buyText: "Adding magic dust to water seems to make a strange brew. Could it be safe to quaff it?",
    tip: "Drinkies",
  },
  rituals: {
    cat: "research",
    price: {wood: 6, magicDust: 6, bone: 6},
    buyLabel: "Ritual altar",
    showButton: isUnlocked("bonfire", "potions"),
    buyText: "Let the rituals begin!",
    tip: "What could possibly go wrong?",
  },
  chalice_wood: {
    cat: "research",
    price: {wood: 1},
    buyLabel: "Wooden chalice",
    showButton: isUnlocked("rituals"),
    buyText: "You fashion a chalice out of wood.",
    tip: "Improves rituals depending on the contents.",
  },
  chalice_copper: {
    cat: "research",
    price: {copper: 1},
    buyLabel: "Copper chalice",
    showButton: isUnlocked("rituals"),
    buyText: "You fashion a chalice out of copper.",
    tip: "Improves rituals depending on the contents.",
  },
  chalice_iron: {
    cat: "research",
    price: {iron: 1},
    buyLabel: "Iron chalice",
    showButton: isUnlocked("rituals"),
    buyText: "You fashion a chalice out of iron.",
    tip: "Improves rituals depending on the contents.",
  },
  chalice_silver: {
    cat: "research",
    price: {silver: 1},
    buyLabel: "Silver chalice",
    showButton: isUnlocked("rituals"),
    buyText: "You fashion a chalice out of silver.",
    tip: "Improves rituals depending on the contents.",
  },
  chalice_gold: {
    cat: "research",
    price: {gold: 1},
    buyLabel: "Gold chalice",
    showButton: isUnlocked("rituals"),
    buyText: "You fashion a chalice out of gold.",
    tip: "Improves rituals depending on the contents.",
  },
  chalice_glass: {
    cat: "research",
    price: {glass: 1},
    buyLabel: "Glass chalice",
    showButton: isUnlocked("rituals"),
    buyText: "You fashion a chalice out of glass.",
    tip: "Improves rituals depending on the contents.",
  },
  troll: {
    cat: "research",
    label: "Trolls",
    showLabel: isUnlocked("trolls"),
    showButton: Never,
  },
  spellbook: {},
  alchemy: {},
  crafting: {},
}
DATA.unlockTriggers = {
  start:      () =>  UI.text(DATA.intro),
  gnome:      () => {let g = UTIL.randGend()
                     UI.text(`As you gather mushrooms in the forest, a gnome approaches you and asks you for some of those red ones with white spots. ${g[0]} will chop some lumber for you if you feed ${g[1].toLowerCase()} some of those.`)},
  farm1:      () =>  UI.text("The gnomes think with some wood you could build a neat mushroom farm to provide them with amanitas."),
  belladona:  () => {UI.text("You notice some gnomes bring back belladona buds from the forest. You follow them and find a patch of it.")
                     GAME.resources.belladona += 0.99},
  fairy:      () =>  UI.text("The belladona flowers have attracted some fairies. You could use them to get some magic powder."),
  bone:       () => {UI.text("You found one of the fairies gnawing on a bone. You're not sure where it came from.")
                     GAME.resources.gnome -= 1
                     GAME.resources.bone += 1},
  bonfire:    () =>  GAME.resources.bonfire += 9,
  rituals:    () =>  ACT.newRitual(),
  village:    () =>  UI.text("A nearby village has noticed your ritual."),
  mines:      () =>  UI.text("You can now build mines."),
}
DATA.unlockChecks = {
  start:      Always,
  gnome:      () => GAME.resources.amanita >= 6,
  farm1:      () => GAME.resources.gnome >= 5,
  belladona:  () => GAME.resources.gnome >= 15 && GAME.resources.wood >= 100,
  fairy:      () => GAME.resources.belladona >= 10,
  magicDust:  () => GAME.resources.magicDust >= 1,
  bone:       () => GAME.resources.fairy >= 2 && (1/200 > Math.random()),
  cats:       () => GAME.resources.cat >= 1,
  trolls:     () => GAME.resources.troll >= 1,
  imps:       () => GAME.resources.imp >= 1,
  goblins:    () => GAME.resources.goblin >= 1,
  kobolds:    () => GAME.resources.kobold >= 1,
  mines:      () => GAME.isUnlocked("goblins"),
}
DATA.ritualSymbolWeights = {
  mercury:      () => GAME.isUnlocked("planetary") ? 10 : 0,
  venus:        () => GAME.isUnlocked("planetary") ? 10 : 0,
  earth:        () => GAME.isUnlocked("planetary") ? 10 : 0,
  mars:         () => GAME.isUnlocked("planetary") ? 10 : 0,
  jupiter:      () => GAME.isUnlocked("planetary") ? 10 : 0,
  saturn:       () => GAME.isUnlocked("planetary") ? 10 : 0,
  uranus:       () => GAME.isUnlocked("planetary") ? 10 : 0,
  neptune:      () => GAME.isUnlocked("planetary") ? 10 : 0,
  pluto:        () => GAME.isUnlocked("planetary") ? 10 : 0,
  quintessence: () => GAME.isUnlocked("alchemy2") ? 10 : 0,
  aer:          () => 9 + Math.log(9 + GAME.elementalAffinity.aer),
  ignis:        () => 9 + Math.log(9 + GAME.elementalAffinity.ignis),
  terra:        () => 9 + Math.log(9 + GAME.elementalAffinity.terra),
  aqua:         () => 9 + Math.log(9 + GAME.elementalAffinity.aqua),
  aquafortis:   () => GAME.isUnlocked("alchemy2") ? 10 : 0,
  aquaregia:    () => GAME.isUnlocked("alchemy2") ? 10 : 0,
  aquavitae:    () => GAME.isUnlocked("alchemy2") ? 10 : 0,
  brimstone:    () => GAME.resources.cat * 5,
  vitriol:      () => GAME.isUnlocked("alchemy") ? 10 : 0,
  salt:         () => GAME.isUnlocked("alchemy") ? 10 : 0,
  gold:         () => GAME.isUnlocked("alchemy") ? 10 : 0,
  silver:       () => GAME.isUnlocked("alchemy") ? 10 : 0,
  iron:         () => GAME.isUnlocked("alchemy") ? 10 : 0,
  copper:       () => GAME.isUnlocked("alchemy") ? 10 : 0,
  tin:          () => GAME.isUnlocked("alchemy") ? 10 : 0,
  lead:         () => GAME.isUnlocked("alchemy") ? 10 : 0,
  antimony:     () => GAME.isUnlocked("alchemy") ? 10 : 0,
  wax:          () => GAME.isUnlocked("alchemy") ? 10 : 0,
  ankh:         () => GAME.isUnlocked("alchemy2") ? 10 : 0,
  aries:        () => GAME.ritualState.alignment == "aries" ? 10 : 1,
  taurus:       () => GAME.ritualState.alignment == "taurus" ? 10 : 1,
  gemini:       () => GAME.ritualState.alignment == "gemini" ? 10 : 1,
  cancer:       () => GAME.ritualState.alignment == "cancer" ? 10 : 1,
  leo:          () => GAME.isUnlocked("cats") ?
                        (GAME.ritualState.alignment == "leo" ?
                          10 + GAME.luck.bad :
                          1) :
                        100,
  virgo:        () => GAME.ritualState.alignment == "virgo" ? 10 : 1,
  libra:        () => GAME.ritualState.alignment == "libra" ? 10 : 1,
  scorpio:      () => GAME.ritualState.alignment == "scorpio" ? 10 : 1,
  sagittarius:  () => GAME.ritualState.alignment == "sagittarius" ? 10 : 1,
  capricorn:    () => GAME.ritualState.alignment == "capricorn" ? 10 : 1,
  aquarius:     () => GAME.ritualState.alignment == "aquarius" ? 10 : 1,
  pisces:       () => GAME.ritualState.alignment == "pisces" ? 10 : 1,
}
DATA.potionIngredients = [
  "water",
  "magicDust",
  "amanita",
  "belladona",
  "slime",
  "wormwood",
  "sulfur",
]
DATA.potions = {
  aer: {
    label: "Airy potion",
    ingredients: {magicDust: 1, water: 2, wormwood: 1},
  },
  aqua: {
    label: "Aqueous potion",
    ingredients: {magicDust: 1, water: 2, slime: 1},
  },
  ignis: {
    label: "Firey potion",
    ingredients: {magicDust: 1, water: 2, sulfur: 1},
  },
  terra: {
    label: "Earthy potion",
    ingredients: {magicDust: 1, water: 2, amanita: 1},
  },
  quine: {
    label: "Quine potion",
    ingredients: {quinePotion: 1}
  },
}
DATA.incense = {}
DATA.candles = {}
DATA.crystals = {}
DATA.chalice = {}
DATA.ritualModifiers = {
  candles: ["aer", "ignis", "aqua", "terra"],
  incense: ["aer", "ignis", "aqua", "terra"],
}
DATA.cursedRituals = ["brimstone"]
