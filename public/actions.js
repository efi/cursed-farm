const ACT = {}
// factor: how many seconds to reach 1
// amount: how many actors there are; factor: how strong the diminishing return is
ACT.linear = (factor, dt) => dt / factor
ACT.diminishing = (amount, factor, dt) => Math.log(amount / 10 + 1) * 10 * dt / factor
ACT.tick = (dt) => {
  // Belladona decay
  if (GAME.resources.belladona >= 1) {GAME.resources.belladona -= ACT.linear(10, dt)}

  // Bonfire decay
  if (GAME.resources.bonfire >= 1) {GAME.resources.bonfire -= ACT.linear(20, dt)}

  // Gnome worker
  if (GAME.resources.gnome >= 1 && GAME.resources.amanita > 0.8) {
    GAME.resources.amanita -= ACT.linear(12, dt) * UTIL.posInt(GAME.resources.gnome)
    GAME.resources.wood += ACT.diminishing(UTIL.posInt(GAME.resources.gnome), 5.5, dt)
  }

  // Amanita farm growth
  if (GAME.resources.amanitaFarm >= 1) {GAME.resources.amanita += ACT.diminishing(UTIL.posInt(GAME.resources.amanitaFarm), 2.5, dt)}
  // Fairy generate dust
  if (GAME.resources.fairy >= 1 && GAME.resources.belladona >= 5) {
    GAME.resources.magicDust += ACT.diminishing(UTIL.posInt(GAME.resources.belladona), 50, dt) * UTIL.posInt(GAME.resources.fairy)
  }

  // Fairy decay/escape
  let n = UTIL.posInt(GAME.resources.fairy)
  let p = 1/1000
  if (GAME.resources.fairy > 500) { // Pulling too many randoms is slow, so we approximate
    let r = 2 * Math.random() - 0.5 // [-1, 1]
    // Binomial Distribution + Bernoulli: Each n has p chance to count. r is the variance on this averaged process.
    GAME.resources.fairy -= Math.max(0,           // minimum zero, no negatives
                              Math.floor(         // integer part
                                n * p +           // mean expected value
                                n * p * (1 - p) * // variance of the expected value
                                r))               // randomization over the variance * [-1, 1]
  } else { // Count the successes, if there are none it'd be undefined, so default to number 0
    GAME.resources.fairy -= _.countBy(_.times(n, () => p > Math.random())).true || 0
  }

  // Temperature process
  let ambient = 10
  let current = GAME.currentTemp
  let bonfire = GAME.resources.bonfire
  let tdiff = bonfire - current
  let adiff = current - ambient
  GAME.currentTemp += dt * Math.max(0, tdiff * 0.05) -
                      dt * Math.sign(adiff) * adiff * adiff * 0.002
  // Star Alignment
  GAME.starAlignment = (GAME.starAlignment + (1 / 60) * dt) % 13
}
ACT.buyFn = {
  amanita: () => Math.random() * 3 + 1,
  bone: () => GAME.resources.troll,
}
ACT.buyMany = (i, n) => {
  if (ACT.canBuy(DATA.resources[i].price, n)) {
    let order = Math.log10(n)
    if (order >= 4) {
      _.times(UTIL.posInt(n / Math.pow(10, order)),
        () => {ACT.transfer(i, DATA.resources[i].price, UTIL.posInt(Math.pow(10, order)))})
    } else {
      _.times(UTIL.posInt(n),
        () => {ACT.transfer(i, DATA.resources[i].price, UTIL.posInt(n))})
    }
    UI.text(DATA.resources[i].buyText + (n > 1 ? `(x${n})` : ""))
  }
}
ACT.transfer = (i, price, n=1) => {
  let f = _.isUndefined(ACT.buyFn[i]) ? _.constant(1) : ACT.buyFn[i]
  if (!_.isNull(i)) {
    GAME.resources[i] += UTIL.posInt(f() * n)
    _.each(price, (v, k) => {
      GAME.resources[k] -= UTIL.posInt(UTIL.fnorval(v) * n)
    })
  }
}
ACT.canBuy = (price, n=1) => !_.any(price, (v, k) => (UTIL.fnorval(v) * n) > Math.floor(GAME.resources[k]))
ACT.research = (i) => {
  if (ACT.canBuy(DATA.research[i].price)) {
    ACT.transfer(null, DATA.research[i].price)
    ACT.unlock(i)
    UI.text(DATA.research[i].buyText)
    if (!_.isUndefined(DATA.unlockTriggers[i])) {
      DATA.unlockTriggers[i]()
    }
  }
}
ACT.unlock = (lock) => {if (!GAME.isUnlocked(lock)) {GAME.unlocks.push(lock)}}
ACT.newRitual = () => {
  //console.log("new ritual")
  GAME.ritualLocks = []
  UI.ritualGrid.children().enable()
  UI.ritualClaim.disable()
  GAME.ritualState = {
    temp:       GAME.currentTemp,
    affinity:   GAME.elementalAffinity,
    luck:       GAME.luck,
    alignment:  UTIL.currentZodiac()
  }
  ACT.ritualRoll()
}
ACT.lockRitualSlot = (n) => {
  GAME.ritualLocks.push(n)
  UI.ritualGrid.children().eq(n).disable()
}
ACT.checkRitual = () => {
  let V = []
  let G = UTIL.grid(5, 5)
  let results = []
  let R = $("ritual .ritual")
  R.each((i, e) => {
    V.push($(e).data("v"))
  })
  _.each(V, (v, k) => {
    if (_.isEmpty(results) || results[0] == v) {
      if (v == V[G.E(k)]  && v == V[G.E(G.E(k))])   {
        UI.hi(R.eq(k).add(R.eq(G.E(k))).add(R.eq(G.E(G.E(k)))))
        results.push(v)
      }
      if (v == V[G.S(k)]  && v == V[G.S(G.S(k))])   {
        UI.hi(R.eq(k).add(R.eq(G.S(k))).add(R.eq(G.S(G.S(k)))))
        results.push(v)
      }
      if (v == V[G.SE(k)] && v == V[G.SE(G.SE(k))]) {
        UI.hi(R.eq(k).add(R.eq(G.SE(k))).add(R.eq(G.SE(G.SE(k)))))
        results.push(v)
      }
      if (v == V[G.SW(k)] && v == V[G.SW(G.SW(k))]) {
        UI.hi(R.eq(k).add(R.eq(G.SW(k))).add(R.eq(G.SW(G.SW(k)))))
        results.push(v)
      }
    }
  })
  if (!_.isEmpty(results)) {
    UI.ritualClaim.enable()
  }
  return results
}
ACT.ritualRoll = () => {
  GAME.ritualRolling = true
  UI.unhi($("ritual .ritual"))
  UI.ritualRestart.disable()
  UI.ritualRoll.disable()
  UI.ritualClaim.disable()
  let stag = 0
  $("ritual .ritual").each((i, e) => {
    if (!_.contains(GAME.ritualLocks, i)) {
      let E = $(e)
      let prevSymbol = E.data("v")
      let nextSymbol = UTIL.ritualSymbol()
      E.data("v", nextSymbol)
      setTimeout(() => {
          E.find("label").animate({opacity: 0},
            {done: () => {
                if (_.contains(DATA.cursedRituals, nextSymbol)) {UI.cursed(E)} else {UI.uncursed(E)}
                E.html(`<label>${DATA.globals.symbols[nextSymbol]}</label>`)
                E.find("label").css("opacity", 0).animate({opacity: 1})
              }
            })
        }, 100 * stag)
      stag += 1
    }
  })
  setTimeout(() => {
    ACT.checkRitual()
    UI.ritualRestart.enable()
    UI.ritualRoll.enable()
    GAME.ritualRolling = false
  }, 100 * (stag + 1) + 600)
}
ACT.ritualClaim = () => {
  let results = ACT.checkRitual()
  //console.log(results[0])
  let state = GAME.ritualState
  if (results[0] != "leo" && GAME.resources.cat < 1) {
    UI.text("The ritual failed! Not enough cats.")
  } else {
    let meh = {
      ignis: () => {
        UI.text("Your fire affinity increased.")
        GAME.elementalAffinity.ignis += 1
      },
      terra: () => {
        UI.text("Your earth affnity increased.")
        GAME.elementalAffinity.terra += 1
      },
      aqua: () => {
        UI.text("Your water affnity increased.")
        GAME.elementalAffinity.aqua  += 1
      },
      aer: () => {
        UI.text("Your air affnity increased.")
        GAME.elementalAffinity.aer   += 1
      },
      leo: () => {
        if (GAME.isUnlocked("cats")) {
          if (1/3 > Math.random()) {
            UI.text("You summoned a cat.")
            GAME.resources.cat += 1
          }
        } else {
          UI.text("You summoned a cat.")
          ACT.unlock("cats")
          GAME.resources.cat += 1
        }
      },
      brimstone: () => {
        GAME.luck.bad += 3
        if ((state.temp / 10) / 5 > Math.random()) {
          UI.text("You summoned an imp.")
          GAME.resources.imp += 1
        }
      },
      scorpio: () => {
        UI.text("You summoned a troll.")
        GAME.resources.troll += 1
      },
      taurus: () => {
        UI.text("You summoned a kobold.")
        GAME.resources.kobold += 1
      },
      aries: () => {
        UI.text("You summoned a goblin.")
        GAME.resources.goblin += 1
      },
    }[results[0]]()
  }
  UI.ritualClaim.disable()
  UI.ritualRoll.disable()
  $("ritual .ritual").disable()
}
