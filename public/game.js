// Xenia is real 2020
var _ // Glitch editor hack
$.fn.extend({
  id:       function (id) { return  id ? this.attr("id", id) || "" : this.attr("id") || "" },
  enable:   function ()   { return  this.find("*").addBack().attr("disabled", false) },
  disable:  function ()   { return  this.find("*").addBack().attr("disabled", true) },
  enabled:  function ()   { return !this.attr("disabled") },
  disabled: function ()   { return  this.attr("disabled") },
  visible:  function ()   { return  this.css("display") != "none"},
  hidden:   function ()   { return  this.css("display") == "none"},
})

var GAME = {
  now:                null,
  last:               null,
  working:            false,
  resources:          null,
  unlocks:            [],
  isUnlocked:         (lock) => _.contains(GAME.unlocks, lock),
  relock:             (lock) => GAME.unlocks = _.reject(GAME.unlocks, (l) => l == lock),
  options:            {},
  currentTemp:        18,
  ritualLocks:        [],
  ritualRolling:      false,
  elementalAffinity:  {ignis: 0, aqua: 0, aer: 0, terra: 0},
  luck:               {good: 0, bad: 0},
  starAlignment:      0,
  ritualState:        {},
  cauldronState:      null,

  init: _.once(() => {
    GAME.last          = _.now()
    GAME.resources     = _.mapObject(DATA.resources, _.constant(0))
    GAME.cauldronState = _.mapObject(_.invert(DATA.potionIngredients), _.constant(0))
    UI.init()
    setInterval(GAME.tick, 200)
  }),
  resValue: (r) => {
    if (_.contains(_.keys(DATA.resources), r)) {
      return GAME.resources[r]
    } else {
      console.error(`Non-existent resource ${r} requested!`)
    }
  },
  tick: () => {
    if (GAME.working) {return}
    GAME.working = true
    GAME.now = _.now()
    GAME.processTurn((GAME.now - GAME.last) / 1000)
    GAME.last = GAME.now
    GAME.working = false
  },
  processTurn: (t) => {
    ACT.tick(t)
    GAME.checkUnlocks()
    UI.update()
  },
  checkUnlocks: () => {
    _.each(DATA.unlockChecks, (v, k) => {
      if (!GAME.isUnlocked(k) && v()) {
        ACT.unlock(k)
        if (!_.isUndefined(DATA.unlockTriggers[k])) {
          DATA.unlockTriggers[k]()
        }
      }
    })
  }
}

async function loadScripts () {
  for (var s of ["util.js", "data.js", "actions.js", "UI.js"]) {
    await $.getScript(s)
  }
}

$(() => {loadScripts().then(GAME.init)})

